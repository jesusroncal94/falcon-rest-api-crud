from pip._internal.req import parse_requirements
from setuptools import setup, find_packages


requirements = [str(r.requirement) for r in parse_requirements('requirements.txt', session=False)]

setup(
    name='api',
    description='Falcon REST API',
    version='0.1',
    author='Jesus Roncal',
    author_email='jesusroncal94@gmail.com',
    packages=find_packages(),
    install_requires=requirements,
    entry_points={
        "console_scripts": [
            "api = api.cli:cli",
        ],
    }
)
