import falcon

from api.src.routes.userRoutes import add_routes as add_user_routes
from api.src.middlewares.database import DatabaseSessionManager
from api.src.models.db import init_session


class App(object):
    def __init__(self):
        self.session = init_session()
        self.api = falcon.API(
            middleware=DatabaseSessionManager(session=self.session)
        )
        add_user_routes(self.api)
