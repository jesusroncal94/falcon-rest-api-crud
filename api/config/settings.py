import os
from dotenv import load_dotenv


def load_env(mode):
    BASE_DIR = os.path.abspath(os.path.dirname(__file__))
    dir_env_path = 'env'
    env_path = os.path.join(BASE_DIR, dir_env_path, "{}.env".format(mode))
    load_dotenv(dotenv_path=env_path)
