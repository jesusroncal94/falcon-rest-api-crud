from api.config.settings import load_env


class Context(object):
    def __init__(self, mode):
        self.mode = mode
    
    def load_config(self):
        load_env(mode=self.mode)
