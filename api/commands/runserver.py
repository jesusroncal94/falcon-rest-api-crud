import click
import os
from waitress import serve
from werkzeug.serving import run_simple

from api.config.context import Context


def run_server(mode):
    try:
        from api.main import App
        from api.src.models.db import Base, engine
        
        
        Base.metadata.create_all(engine)
        api = App().api
        if mode in ['dev', 'test']:
            run_simple(
                application=api,
                hostname=os.getenv('HOST'),
                port=int(os.getenv('PORT')),
                use_reloader=True,
                use_debugger=True,
            )
        elif mode in ['prod']:
            serve(
                app=api,
                host=os.getenv('HOST'),
                port=os.getenv('PORT')
            )
    except Exception as e:
        print(str(e))


@click.command()
@click.option('-m', '--mode')
def command(mode):
    """
    Command to run the api in different modes.
    Options for mode argument are dev (development), test or prod (production)
    """
    if mode in ['dev', 'test', 'prod']:
        Context(mode).load_config()
        run_server(mode)
    else:
        print('Please specify a correct mode.')
