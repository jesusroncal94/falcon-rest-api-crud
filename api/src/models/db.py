import os
from dictalchemy import DictableModel
from sqlalchemy import create_engine, event
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import mapper, scoped_session, sessionmaker


# Session
engine = create_engine(os.getenv('DB_URI'), connect_args={'connect_timeout': 10})
session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)

# Base
Base = declarative_base(cls=DictableModel)
Base.query = Session.query_property()


# Listener for auto session.add()
@event.listens_for(mapper, 'init')
def auto_add(target, args, kwargs):
    Session.add(target)


# Return session object
def init_session():
    try:
        return Session
    except Exception as e:
        print(str(e))
