from sqlalchemy import Column, Integer, String

from api.src.models.db import Base


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    username = Column(String(250), nullable=False, unique=True)
    email = Column(String(250), nullable=False, unique=True)
    firstName = Column(String(150), nullable=False)
    lastName = Column(String(200), nullable=False)
    age = Column(Integer, nullable=True)

    def __init__(self, username, email, firstName, lastName, age=''):
        self.username = username
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
        self.age = age

    def __repr__(self):
        return f'User({self.firstName}, {self.lastName})'

    def __str__(self):
        return f'{self.firstName} {self.lastName}'
