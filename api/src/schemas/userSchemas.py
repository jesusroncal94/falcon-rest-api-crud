from marshmallow import Schema, fields, validate

from api.src.schemas import regexs


class UserSchema(Schema):
    id = fields.Integer()
    username = fields.String(
        required=True,
        validate=[
            validate.Regexp(
                regexs.user_username,
                error='Formato de username inválido.'
            ),
            validate.Length(
                min=1,
                error='Se requiere el username.'
            )
        ],
        error_messages={
            'required': ['Se requiere el username.']
        }
    )
    email = fields.String(
        required=True,
        validate=[
            validate.Regexp(
                regexs.user_email,
                error='Formato de email inválido.'
            ),
            validate.Length(
                min=1,
                error='Se requiere el email.'
            )
        ],
        error_messages={
            'required': ['Se requiere el email.']
        }
    )
    firstName = fields.String(
        required=True,
        validate=[
            validate.Regexp(
                regexs.user_firstName,
                error='Formato de nombre inválido.'
            ),
            validate.Length(
                min=1,
                error='Se requiere el nombre.'
            )
        ],
        error_messages={
            'required': ['Se requiere el nombre.']
        }
    )
    lastName = fields.String(
        required=True,
        validate=[
            validate.Regexp(
                regexs.user_lastName,
                error='Formato de apellido inválido.'
            ),
            validate.Length(
                min=1,
                error='Se requiere el apellido.'
            )
        ],
        error_messages={
            'required': ['Se requiere el apellido.']
        }
    )
    age = fields.Integer(
        required=True,
        error_messages={
            'required': ['Se requiere la edad.'],
        }
    )
