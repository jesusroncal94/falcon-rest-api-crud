user_username = r"^[a-zA-Z0-9]{1,250}$"
user_email = r"^[a-zA-Z0-9]+[\._]?[a-zA-Z0-9]+[@]\w+[.]\w{2,3}$"
user_firstName = r"^[a-zA-Z]{1,150}$"
user_lastName = r"^[a-zA-Z]{1,200}$"
