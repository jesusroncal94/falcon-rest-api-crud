def handler_response(resp, message):
    """
    Restructure the response into a more readable one
    """
    
    if not resp.media:
        resp.media = {}

    # Struct the response
    response_object = {
        'response': {
            'systemMessage': message,
            'apiResponse': resp.media,
            'statusCode': resp.status,
        }
    }

    # Update the response (body)
    resp.media = response_object

    return response_object
