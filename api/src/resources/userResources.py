import falcon
from marshmallow import ValidationError
from sqlalchemy import and_, or_

from api.src.models.user import User
from api.src.schemas.userSchemas import UserSchema
from api.src.utils.queryUtils import object_as_dict
from api.src.utils.responses import handler_response


class UserCollectionResource():
    def on_get(self, req, resp):
        session = req.context.session
        try:
            query = User.query.all()
            users = [user.asdict() for user in query]
            resp.media = users
            message = 'List of users.'
            handler_response(resp, message)
        except Exception as e:
            session.rollback()
            resp.media = str(e)
            resp.status = falcon.HTTP_BAD_REQUEST
            handler_response(resp, str(e))
        finally:
            session.close()


    def on_post(self, req, resp):
        session = req.context.session
        try:
            new_user_data = req.media
            data_validated = UserSchema().load(new_user_data)

            # Validate duplicated username or email
            username = data_validated['username']
            email = data_validated['email']
            query = User.query.filter(or_(
                User.username==username,
                User.email==email)).first()
            if query:
                if query.username == username:
                    raise Exception('Username has already by other user.')
                if query.email == email:
                    raise Exception('Email has already by other user.')

            new_user = User(**data_validated)
            session.commit()
            resp.media = new_user.asdict()
            message = 'User created successfully.'
            handler_response(resp, message)
        except ValidationError as e:
            resp.media = e.messages
            resp.status = falcon.HTTP_BAD_REQUEST
            handler_response(resp, e.messages)
        except Exception as e:
            session.rollback()
            resp.media = str(e)
            resp.status = falcon.HTTP_BAD_REQUEST
            handler_response(resp, str(e))
        finally:
            session.close()


class UserSingleResource():
    def on_get(self, req, resp, id):
        session = req.context.session
        try:
            user_id = id
            if not user_id:
                raise Exception('User ID param not found.')

            query = User.query.filter_by(id=user_id).first()
            if(not query):
                raise Exception('User not found.')

            user = query.asdict()
            resp.media = user
            message = 'User got successfully.'
            handler_response(resp, message)
        except Exception as e:
            session.rollback()
            resp.media = str(e)
            resp.status = falcon.HTTP_BAD_REQUEST
            handler_response(resp, str(e))
        finally:
            session.close()


    def on_put(self, req, resp, id):
        session = req.context.session
        try:
            user_id = id
            if not user_id:
                raise Exception('User ID param not found.')

            user_data = req.media
            data_validated = UserSchema(partial=True).load(user_data)
            query = User.query.filter_by(id=user_id).first()
            if(not query):
                raise Exception('User not found.')

            # Validate duplicated username or email
            username = data_validated['username']
            email = data_validated['email']
            query = User.query.filter(and_(
                User.id != user_id,
                or_(
                    User.username==username,
                    User.email==email))).first()
            if query:
                if query.username == username:
                    raise Exception('Username has already by other user.')
                if query.email == email:
                    raise Exception('Email has already by other user.')

            User.query\
                .filter_by(id=user_id)\
                .update(data_validated)
            session.commit()
            data_validated.update({'id': user_id})
            updated_user = data_validated
            resp.media = updated_user
            message = 'User updated successfully.'
            handler_response(resp, message)
        except ValidationError as e:
            resp.media = e.messages
            resp.status = falcon.HTTP_BAD_REQUEST
            handler_response(resp, e.messages)
        except Exception as e:
            session.rollback()
            resp.media = str(e)
            resp.status = falcon.HTTP_BAD_REQUEST
            handler_response(resp, str(e))
        finally:
            session.close()


    def on_delete(self, req, resp, id):
        session = req.context.session
        try:
            user_id = id
            if not user_id:
                raise Exception('User ID param not found.')

            query = User.query.filter_by(id=user_id).first()
            if(not query):
                    raise Exception('User not found.')

            User.query\
                .filter_by(id=user_id)\
                .delete()
            session.commit()
            message = 'User deleted successfully.'
            handler_response(resp, message)
        except Exception as e:
            session.rollback()
            resp.media = str(e)
            resp.status = falcon.HTTP_BAD_REQUEST
            handler_response(resp, str(e))
        finally:
            session.close()
