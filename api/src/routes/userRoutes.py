from api.src.resources.userResources import UserCollectionResource, UserSingleResource


def add_routes(api):
    api.add_route('/users', UserCollectionResource())
    api.add_route('/users/{id}', UserSingleResource())
