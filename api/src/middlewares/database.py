from falcon import HTTP_BAD_REQUEST

from api.src.utils.responses import handler_response


# Middleware for managing session
class DatabaseSessionManager(object):
    def __init__(self, session, auto_commit=False):
        self.session = session
        self.auto_commit = auto_commit
    
    # Pass session in req.context
    def process_request(self, req, resp):
        req.context.session = self.session


    # If auto_commit is True Session will commit or rollback
    def process_response(self, req, resp, resource, req_succeeded):
        session = req.context.session
        if self.auto_commit:
            try:
                session.commit()
            except Exception as e:
                session.rollback()
                resp.media = str(e)
                resp.status = HTTP_BAD_REQUEST
                handler_response(resp, str(e))
