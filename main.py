from waitress import serve

from api.src import App
from api.src.models.db import Base, engine


if __name__ == "__main__":
    try:
        Base.metadata.create_all(engine)
        api = App().api
        serve(api, host='0.0.0.0', port=8000)
    except Exception as e:
        print(str(e))
